package ru.jied83.test.ui

import junit.framework.Assert.assertEquals
import org.junit.Test
import ru.jied83.test.data.remote.dto.*
import ru.jied83.test.data.remote.mappers.MerchantInfoMapper
import ru.jied83.test.model.TagType
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.merchant.MerchantInfo
import ru.jied83.test.ui.merchant.MerchantInfoViewElementMapper

class MerchantDetailsViewModelMapperTest {

    private val vmMapper = MerchantInfoViewElementMapper()
    private val toMapper = MerchantInfoMapper()
    private val addressTO = AddressTo("country", "street", "zipcode", "city", "number")
    private val tagTO = TagContainerTo(TagType.CUISINE.toString(), listOf(TagTo("id", "russian")))
    private val infoTo = MerchantInformationTo(
        "1",
        "2",
        listOf(ImageTo("url")),
        LocationTo(
            CoordinatesTo(lat = 22.3, lng = 33.2),
            addressTO
        ),
        "6",
        listOf(),
        tags = listOf(tagTO)
    )
    val mapped = vmMapper.invoke(toMapper.invoke(infoTo))

    @Test
    fun merchantHeaderTest() {
        val header = mapped[0] as MerchantInfo.ImageHeader
        assertEquals(infoTo.images?.map { it.url }, header.imageUrl)
    }

    @Test
    fun merchantInfoTest() {
        val info = mapped[1] as MerchantInfo.Information
        assertEquals(infoTo.name, info.name)
    }

    @Test
    fun merchantSectionTest() {
        val section = mapped[2] as ViewElement.SectionHeader
        assertEquals("Cuisines", section.title)
    }

    @Test
    fun merchantCuisinesSectionTest() {
        val cuisines = mapped[3] as MerchantInfo.Cuisine
        assertEquals(tagTO.tags.map { it.name }, cuisines.cousines)
    }
}