package ru.jied83.news.ui

import junit.framework.Assert.assertEquals
import org.junit.Test
import ru.jied83.test.model.Merchant
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.merchants.MerchantsViewElementMapper
import ru.jied83.test.ui.merchants.predicate

class MerchantsViewElementMapperTest {

    private val mapper = MerchantsViewElementMapper()
    private val merch = Merchant("id", "name", listOf("url"))

    @Test
    fun shouldMapToViewElement() {
        val element = mapper.invoke(listOf(merch))[0] as ViewElement.Merchant
        assertEquals(merch.id, element.id)
        assertEquals(merch.name, element.name)
        assertEquals(merch.images?.get(0), element.imageUrl)
    }

    @Test
    fun predicateTest() {
        assertEquals(true, predicate(7))
        assertEquals(false, predicate(1))
        assertEquals(false, predicate(2))
        assertEquals(false, predicate(3))
        assertEquals(true, predicate(0))
    }
}