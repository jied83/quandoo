package ru.jied83.test.data

import junit.framework.Assert.assertEquals
import org.junit.Assert
import org.junit.Test
import ru.jied83.test.data.remote.dto.ImageTo
import ru.jied83.test.data.remote.dto.MerchantTo
import ru.jied83.test.data.remote.dto.MerchantsResponse
import ru.jied83.test.data.remote.mappers.SearchResultMapper

class SearchResultMapperTests {

    private val mapper = SearchResultMapper()
    private val merchTo = MerchantTo("1", "2", listOf(ImageTo("3")))
    private val response = MerchantsResponse(listOf(merchTo), 0, 1, 1)

    @Test
    fun shouldMapSearchResult() {
        val query = "query"
        val result = mapper.invoke(response, query)

        Assert.assertEquals(1, result.merchants.size)
        val merch = result.merchants[0]
        assertEquals(merchTo.id, merch.id)
        assertEquals(merchTo.name, merch.name)
        assertEquals(merchTo.images[0].url, merch.images?.get(0))
        assertEquals(merchTo.id, merch.id)
        assertEquals(query, result.query)
    }
}