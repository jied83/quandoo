package ru.jied83.test.data

import junit.framework.Assert.assertEquals
import org.junit.Test
import ru.jied83.test.data.remote.dto.*
import ru.jied83.test.data.remote.mappers.MerchantInfoMapper
import ru.jied83.test.model.TagType


class MerchantInfoMapperTests {

    private val mapper = MerchantInfoMapper()
    private val addressTO = AddressTo("country", "street", "zipcode", "city", "number")
    private val tagTO = TagContainerTo(TagType.CUISINE.toString(), listOf(TagTo("id", "russian")))
    private val infoTo = MerchantInformationTo(
        "1",
        "2",
        listOf(ImageTo("url")),
        LocationTo(
            CoordinatesTo(lat = 22.3, lng = 33.2),
            addressTO
        ),
        "6",
        listOf(),
        tags = listOf(tagTO)
    )

    @Test
    fun shouldMapGeneralInfo() {
        val result = mapper.invoke(infoTo)
        assertEquals(infoTo.id, result.id)
        assertEquals(infoTo.name, result.name)
        assertEquals(infoTo.reviewScore, result.rating)
        assertEquals(infoTo.images?.get(0)?.url, result.images?.get(0))
    }

    @Test
    fun shouldMapAddressTest() {
        val result = mapper.invoke(infoTo)
        val address = result.address
        assertEquals(addressTO.city, address?.city)
        assertEquals(addressTO.country, address?.country)
        assertEquals(addressTO.zipcode, address?.zip)
        assertEquals("${addressTO.street} ${addressTO.number}", address?.address)

    }

    @Test
    fun shouldMapCuisinesTest() {
        val result = mapper.invoke(infoTo)
        val tag = result.cuisines
        assertEquals(tagTO.tags[0].name, tag?.get(0))
    }
}