package ru.jied83.test.data

import junit.framework.Assert.assertEquals
import org.junit.Assert
import org.junit.Test
import ru.jied83.test.data.remote.dto.ReviewResponse
import ru.jied83.test.data.remote.dto.ReviewTo
import ru.jied83.test.data.remote.mappers.ReviewMapper

class ReviewMapperTests {

    private val mapper = ReviewMapper()
    private val reviewTo = ReviewTo("1", "2", "3", "4", 5.0, "6")
    private val response = ReviewResponse(listOf(reviewTo), 0, 1, 1)

    @Test
    fun shouldMapSearchResult() {
        val result = mapper.invoke(response)
        Assert.assertEquals(1, result.size)
        val review = result[0]
        assertEquals(reviewTo.description, review.description)
        assertEquals(reviewTo.rating, review.rating)
        assertEquals("${reviewTo.firstName} ${reviewTo.lastName}", review.name)
    }
}