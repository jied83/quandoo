package ru.jied83.test

class AuthException(msg: String) : Exception(msg)

class ServerException(msg: String) : Exception(msg)