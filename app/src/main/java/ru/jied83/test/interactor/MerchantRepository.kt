package ru.jied83.test.interactor

import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.Merchants
import ru.jied83.test.model.Review

interface MerchantRepository {

    suspend fun fetchMerchants(query: String?, offset: Int, count: Int): Merchants
    suspend fun fetchMerchantDetails(id: String): MerchantDetails
    suspend fun fetchReview(id: String): List<Review>
}