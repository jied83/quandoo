package ru.jied83.test.interactor

import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.Merchants
import ru.jied83.test.model.Review

interface MerchantInteractor {

    suspend fun fetchMerchants(request: MerchantRequest): Merchants
    suspend fun fetchMerchantDetails(id: String): MerchantDetails
    suspend fun fetchReview(id: String): List<Review>
}

sealed class MerchantRequest {

    companion object {
        const val PAGE_SIZE = 30
    }

    data class Query(
        val query: String,
        val offset: Int,
        val count: Int = PAGE_SIZE
    ) : MerchantRequest()
}