package ru.jied83.test.interactor

import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.Merchants
import ru.jied83.test.model.Review


class QuandooMerchantInteractor(
    private val merchantsRepository: MerchantRepository
) : MerchantInteractor {

    override suspend fun fetchMerchants(request: MerchantRequest): Merchants =
        when (request) {
            is MerchantRequest.Query -> merchantsRepository.fetchMerchants(
                query = request.query,
                offset = request.offset,
                count = request.count
            )
        }

    override suspend fun fetchMerchantDetails(id: String): MerchantDetails =
        merchantsRepository.fetchMerchantDetails(id)

    override suspend fun fetchReview(id: String): List<Review> =
        merchantsRepository.fetchReview(id)
}