package ru.jied83.test.model


data class Merchants(
    val query: String?,
    val offset: Int,
    val count: Int,
    val merchants: List<Merchant>)