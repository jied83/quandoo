package ru.jied83.test.model

data class Merchant(
    val id: String,
    val name: String?,
    val images: List<String>?
)

data class MerchantDetails(
    val id: String,
    val name: String,
    val images: List<String>?,
    val address: Address?,
    val rating: String?,
    val infoLink: String?,
    val cuisines: List<String>?
)

data class Review(
    val name: String,
    val rating: Double,
    val description: String?
)

data class Address(
    val country: String?,
    val zip: String?,
    val city: String?,
    val address: String?
)