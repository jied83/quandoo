package ru.jied83.test

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import ru.jied83.test.di.application
import ru.jied83.test.di.navigation
import ru.jied83.test.di.ui

class TestApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@TestApplication)
            modules(listOf(application, navigation, ui))
        }
    }
}