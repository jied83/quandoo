package ru.jied83.test.ui.merchants.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.ui.common.*

class TestRecyclerAdapter(urlCallback: urlCallback) :
    ListAdapter<ViewElement, RecyclerView.ViewHolder>(DiffUtilCallback()) {

    companion object {
        const val TYPE_MERCHANT = 1
        const val TYPE_PROGRESS = 2
        const val TYPE_ERROR = 3
    }

    private val currentList: MutableList<ViewElement> = mutableListOf()
    private val adapters = mutableMapOf<Int, Adapter<ViewElement>>()
    private val itemInsertedAdapterCallback = AssistantDataObserver(this)


    init {
        registerAdapterDataObserver(itemInsertedAdapterCallback)
        registerAdapter(MechantAdapterDelegate(urlCallback), TYPE_MERCHANT)
        registerAdapter(ProgressAdapterDelegate(), TYPE_PROGRESS)
        registerAdapter(SearchErrorOrEmptyAdapterDelegate(), TYPE_ERROR)
        submitList(mutableListOf())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return adapters[viewType]?.onCreateViewHolder(parent)!!
    }

    override fun getItemViewType(position: Int): Int {
        for ((key, value) in adapters) {
            if (value.isForViewType(currentList, position)) return key
        }
        throw IllegalStateException("no adapter or wrong viewtype")
    }

    fun set(list: List<ViewElement>) {
        currentList.clear()
        currentList.addAll(list)
        submitList(currentList)
        notifyDataSetChanged()
    }

    private fun getDelegateForPosition(position: Int): Adapter<ViewElement> {
        for (adapter in adapters.values) {
            if (adapter.isForViewType(currentList, position))
                return adapter
        }
        throw IllegalStateException("no adapter or wrong viewtype")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getDelegateForPosition(position).onBindViewHolder(currentList, position, holder)
    }

    private fun registerAdapter(adapter: Adapter<ViewElement>, type: Int) {
        adapters[type] = adapter
    }

    inner class AssistantDataObserver(adapter: RecyclerView.Adapter<*>) :
        RecyclerView.AdapterDataObserver() {

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {

        }
    }
}

class DiffUtilCallback : DiffUtil.ItemCallback<ViewElement>() {

    override fun areItemsTheSame(oldItem: ViewElement, newItem: ViewElement): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ViewElement, newItem: ViewElement): Boolean {
        return oldItem == newItem
    }

}