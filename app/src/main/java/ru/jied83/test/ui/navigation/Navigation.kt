package ru.jied83.test.ui.navigation

import androidx.fragment.app.Fragment
import ru.jied83.test.R
import ru.jied83.test.ui.merchant.MerchantDetailsFragment
import ru.jied83.test.ui.merchants.MerchantsFragment


interface Navigation {

    fun merchants()
    fun merchantInfo(id: String)
}

class TestNavigation(private val activityNavigation: ActivityHolder) : Navigation {
    override fun merchants() {
        changeFragment(MerchantsFragment.newInstance())
    }

    override fun merchantInfo(id: String) {
        changeFragment(MerchantDetailsFragment.newInstance(id), addToBackStack = true)
    }

    private fun changeFragment(
        fragment: Fragment,
        addToBackStack: Boolean = false,
        clearBackStack: Boolean = false,
        tag: String? = null
    ) {
        activityNavigation.activity?.supportFragmentManager?.let { fragmentManager ->
            if (clearBackStack) {
                while (fragmentManager.backStackEntryCount > 0) {
                    fragmentManager.popBackStackImmediate()
                }
            }
            val transaction = fragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                android.R.animator.fade_in,
                android.R.animator.fade_out
            )
            transaction.replace(R.id.container, fragment, tag)
            if (addToBackStack) {
                transaction.addToBackStack("")
            }
            transaction.commit()
        }
    }

}

sealed class NavigationRequest {

    data class showMerchantInfo(val id: String) : NavigationRequest()
    object back : NavigationRequest()
}