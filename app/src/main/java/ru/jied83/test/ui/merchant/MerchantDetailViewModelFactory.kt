package ru.jied83.test.ui.merchant

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.jied83.test.interactor.MerchantInteractor

@Suppress("UNCHECKED_CAST")
class MerchantDetailViewModelFactory(

    private val merchantInteractor: MerchantInteractor,
    private val mapper: MerchantInfoViewElementMapper,
    private val reviewMapper: MerchantReviewViewElementMapper
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MerchantDetailsViewModel(merchantInteractor, mapper, reviewMapper) as T
    }
}