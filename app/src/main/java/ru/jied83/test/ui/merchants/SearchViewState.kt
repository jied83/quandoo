package ru.jied83.test.ui.merchants

import ru.jied83.test.ui.common.ViewElement

sealed class SearchViewState {

    abstract val elements: List<ViewElement>
    abstract val search: String

    data class LoadingProcess(
        override val elements: List<ViewElement>,
        override val search: String,
        val initial: Boolean
    ) : SearchViewState()

    data class SearchSuccess(
        override val elements: List<ViewElement>,
        override val search: String,
        val offset: Int,
        val count: Int
    ) : SearchViewState()

    data class SearchFailure(
        override val elements: List<ViewElement>,
        override val search: String
    ) : SearchViewState()
}