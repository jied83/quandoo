package ru.jied83.test.ui.merchant.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.jied83.test.R
import ru.jied83.test.ui.common.Adapter
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.merchant.MerchantInfo

class MerchantInfoHeaderAdapterDelegate : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is MerchantInfo.ImageHeader
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
        val merchantHeaderHolder = holder as MerchantInfoHeaderItemViewHolder
        val merchantHeaderElement = list[position] as MerchantInfo.ImageHeader
        merchantHeaderHolder.bind(merchantHeaderElement)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MerchantInfoHeaderItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_merchant_info_header,
                parent,
                false
            )
        )
    }

    class MerchantInfoHeaderItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val coverList: RecyclerView = view.findViewById(R.id.coverList)
        private val lManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
        private val cover: ImageView = view.findViewById(R.id.cover)
        private val snapHelper = LinearSnapHelper()

        fun bind(data: MerchantInfo.ImageHeader) {
            data.imageUrl?.let { url ->
                cover.visibility = View.GONE
                val imageAdapter = CoverListAdapter(data.imageUrl)
                snapHelper.attachToRecyclerView(coverList)
                coverList.apply {
                    layoutManager = lManager
                    adapter = imageAdapter
                }
            } ?: run {
                cover.setImageResource(R.drawable.image_placeholder)
                cover.visibility = View.VISIBLE
            }
        }
    }
}

class CoverListAdapter(private val data: List<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImageItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_image,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val h = holder as ImageItemViewHolder
        h.bind(data[position])
    }

    class ImageItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val cover: ImageView = view.findViewById(R.id.cover)
        private val glide = Glide.with(view.context)
        private val requestOptions = RequestOptions().apply {
            placeholder(R.drawable.image_placeholder)
            error(R.drawable.image_placeholder)
        }

        fun bind(data: String?) {
            data?.let { url ->
                glide.load(url).apply(requestOptions).into(cover)
            } ?: cover.setImageResource(R.drawable.image_placeholder)
        }
    }
}