package ru.jied83.test.ui.merchant

import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.Review
import ru.jied83.test.ui.common.ViewElement

class MerchantInfoViewElementMapper : (MerchantDetails) -> List<ViewElement> {

    override fun invoke(m: MerchantDetails): List<ViewElement> {
        val image = if (m.images?.isNotEmpty() == true) m.images else null

        val cuisines = m.cuisines?.takeIf { it.isNotEmpty() }?.let {
            listOf(ViewElement.SectionHeader("Cuisines"), MerchantInfo.Cuisine(it))
        } ?: listOf()

        return listOf(
            MerchantInfo.ImageHeader(image),
            MerchantInfo.Information(
                name = m.name,
                addressLine = m.address?.address,
                cityLine = m.address?.run {
                    "${zip.orEmpty()} ${city.orEmpty()} ${country.orEmpty()}".trim()
                },
                rating = "${m.rating}/6"
            )
        ) + cuisines
    }
}

class MerchantReviewViewElementMapper : (List<Review>) -> List<ViewElement> {
    override fun invoke(reviews: List<Review>): List<ViewElement> =
        reviews.map {
            MerchantInfo.Review(
                name = it.name,
                description = it.description,
                rating = it.rating.toString()
            )
        }
}

sealed class MerchantInfo : ViewElement.Extended() {

    data class ImageHeader(val imageUrl: List<String>?) : MerchantInfo()

    data class Information(
        val name: String,
        val addressLine: String?,
        val cityLine: String?,
        val rating: String?
    ) : MerchantInfo()

    data class Review(
        val name: String,
        val rating: String,
        val description: String?
    ) : MerchantInfo()

    data class Cuisine(val cousines: List<String>) : MerchantInfo()
}
