package ru.jied83.test.ui.merchants

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.currentScope
import ru.jied83.test.R
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.getOrientation
import ru.jied83.test.ui.merchants.adapters.TestRecyclerAdapter
import ru.jied83.test.ui.navigation.Navigation
import ru.jied83.test.ui.navigation.NavigationRequest

class MerchantsFragment : Fragment() {

    companion object {
        fun newInstance() = MerchantsFragment()
    }

    private lateinit var viewModel: MerchantsViewModel
    private lateinit var recyclerAdapter: TestRecyclerAdapter
    private lateinit var gridLayoutManager: GridLayoutManager
    private val viewModelFactory: MerchantsViewModelFactory by currentScope.inject()
    private val navigator: Navigation by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(MerchantsViewModel::class.java)
        setupRecyclerview()
        setUpToolbar()
        viewModel.searchViewState.observe(viewLifecycleOwner, Observer(::applyState))
        viewModel.navigationRequest.observe(viewLifecycleOwner, Observer(::navigate))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { viewModel.onSearchInput(query) }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrBlank()) {
                    viewModel.onSearchInput("")
                }
                return true
            }
        })
    }

    private fun setupRecyclerview() {
        recyclerAdapter = TestRecyclerAdapter { url ->
            viewModel.onItemClick(url)
        }
        gridLayoutManager = setupGridLayoutManager()
        recycler.apply {
            this.layoutManager = gridLayoutManager
            this.adapter = recyclerAdapter
            this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val totalItemCount = gridLayoutManager.itemCount
                    val lastVisible = gridLayoutManager.findLastVisibleItemPosition()
                    val endHasBeenReached = lastVisible + 5 >= totalItemCount
                    if (totalItemCount > 0 && endHasBeenReached) {
                        viewModel.onLoadMore()
                    }
                }
            })
        }
    }

    private fun applyState(viewstate: SearchViewState) {
        updateList(viewstate.elements)
    }

    private fun navigate(request: NavigationRequest) {
        when (request) {
            is NavigationRequest.showMerchantInfo -> navigator.merchantInfo(request.id)
        }
    }

    private fun setupGridLayoutManager(): GridLayoutManager {
        val spanSize = SpanSizeCalculator().calc(requireContext().getOrientation())
        val spanLookup = GridSpanSizeLookup(spanSize)
        return GridLayoutManager(requireContext(), spanSize).apply {
            spanSizeLookup = spanLookup
        }
    }

    private fun setUpToolbar() {
        with(requireActivity() as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.run {
                title = ""
            }
        }
    }

    private fun updateList(elements: List<ViewElement>) {
        recycler.layoutManager = if (elements.size == 1)
            LinearLayoutManager(requireContext())
        else
            gridLayoutManager
        recyclerAdapter.set(elements)
    }
}
