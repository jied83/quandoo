package ru.jied83.test.ui.merchant.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.ui.common.*
import ru.jied83.test.ui.merchants.adapters.DiffUtilCallback

class MerchantInfoRecyclerAdapter :
    ListAdapter<ViewElement, RecyclerView.ViewHolder>(DiffUtilCallback()) {

    companion object {
        const val TYPE_MERCHANT_HEADER = 1
        const val TYPE_MERCHANT_INFO = 2
        const val TYPE_PROGRESS = 3
        const val TYPE_ERROR = 4
        const val TYPE_REVIEW = 5
        const val TYPE_SECTION_HEADER = 6
        const val TYPE_SECTION_CUISINE = 7
    }

    private val currentList: MutableList<ViewElement> = mutableListOf()
    private val adapters = mutableMapOf<Int, Adapter<ViewElement>>()
    private val itemInsertedAdapterCallback = AssistantDataObserver(this)


    init {
        registerAdapterDataObserver(itemInsertedAdapterCallback)
        registerAdapter(MerchantInfoHeaderAdapterDelegate(), TYPE_MERCHANT_HEADER)
        registerAdapter(MerchantInfoAdapterDelegate(), TYPE_MERCHANT_INFO)
        registerAdapter(MerchantReviewAdapterDelegate(), TYPE_REVIEW)
        registerAdapter(ProgressAdapterDelegate(), TYPE_PROGRESS)
        registerAdapter(SearchErrorOrEmptyAdapterDelegate(), TYPE_ERROR)
        registerAdapter(SectionHeaderAdapterDelegate(), TYPE_SECTION_HEADER)
        registerAdapter(MerchantCuisinesAdapterDelegate(), TYPE_SECTION_CUISINE)
        submitList(mutableListOf())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return adapters[viewType]?.onCreateViewHolder(parent)!!
    }

    override fun getItemViewType(position: Int): Int {
        for ((key, value) in adapters) {
            if (value.isForViewType(currentList, position)) return key
        }
        throw IllegalStateException("no adapter or wrong viewtype")
    }

    fun set(list: List<ViewElement>) {
        currentList.clear()
        currentList.addAll(list)
        submitList(currentList)
        notifyDataSetChanged()
    }

    private fun getDelegateForPosition(position: Int): Adapter<ViewElement> {
        for (adapter in adapters.values) {
            if (adapter.isForViewType(currentList, position))
                return adapter
        }
        throw IllegalStateException("no adapter or wrong viewtype")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getDelegateForPosition(position).onBindViewHolder(currentList, position, holder)
    }

    private fun registerAdapter(adapter: Adapter<ViewElement>, type: Int) {
        adapters[type] = adapter
    }

    inner class AssistantDataObserver(adapter: RecyclerView.Adapter<*>) :
        RecyclerView.AdapterDataObserver() {

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {

        }
    }
}