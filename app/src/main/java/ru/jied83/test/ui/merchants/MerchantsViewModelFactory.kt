package ru.jied83.test.ui.merchants

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.jied83.test.interactor.MerchantInteractor

@Suppress("UNCHECKED_CAST")
class MerchantsViewModelFactory(
    private val merchantInteractor: MerchantInteractor,
    private val mapper: MerchantsViewElementMapper
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MerchantsViewModel(merchantInteractor, mapper) as T
    }
}