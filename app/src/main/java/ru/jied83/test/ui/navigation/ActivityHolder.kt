package ru.jied83.test.ui.navigation

import androidx.fragment.app.FragmentActivity

class ActivityHolder {
    var activity: FragmentActivity? = null

    fun attachActivity(activity: FragmentActivity) {
        this.activity = activity
    }

    fun detachActivity() {
        activity = null
    }
}