package ru.jied83.test.ui.merchants

import ru.jied83.test.model.Merchant
import ru.jied83.test.ui.common.ViewElement

class MerchantsViewElementMapper : (List<Merchant>) -> List<ViewElement> {
    override fun invoke(merchants: List<Merchant>): List<ViewElement> =
        merchants.map {
            ViewElement.Merchant(
                id = it.id,
                name = it.name,
                imageUrl = if (it.images?.isNotEmpty() == true) it.images[0] else null
            )
        }
}

fun predicate(index: Int) = index == 0 || (index) % 7 == 0