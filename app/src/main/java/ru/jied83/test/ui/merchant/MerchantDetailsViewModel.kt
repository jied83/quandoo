package ru.jied83.test.ui.merchant

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.jied83.test.interactor.MerchantInteractor
import ru.jied83.test.ui.ViewModelScope
import ru.jied83.test.ui.common.ViewElement

class MerchantDetailsViewModel(
    private val merchantInteractor: MerchantInteractor,
    private val merchantInfoViewElementMapper: MerchantInfoViewElementMapper,
    private val merchantReviewViewElementMapper: MerchantReviewViewElementMapper
) : ViewModelScope() {

    val viewState: LiveData<MerchantDetailsViewState> get() = _viewState
    private val _viewState = MutableLiveData<MerchantDetailsViewState>()

    fun onCreate(id: String) {
        if (shouldFetch(viewState.value)) {
            launchImmediate {
                _viewState.value =
                    MerchantDetailsViewState.Loading(listOf(ViewElement.Progress))
                runCatching {
                    withContext(Dispatchers.IO) {
                        merchantInteractor.fetchMerchantDetails(id)
                    }
                }.onFailure {
                    handleError(it)
                }.onSuccess {
                    _viewState.value =
                        MerchantDetailsViewState.Success(merchantInfoViewElementMapper(it))
                    fetchReview(id)
                }
            }
        }
    }

    private fun fetchReview(id: String) {
        launchImmediate {
            runCatching {
                withContext(Dispatchers.IO) {
                    merchantInteractor.fetchReview(id)
                }
            }.onFailure {
                //todo add error element
            }.onSuccess {
                val lastState = viewState.value as MerchantDetailsViewState.Success
                _viewState.value = MerchantDetailsViewState.Success(
                    lastState.elements + merchantReviewViewElementMapper(it)
                )
            }
        }
    }

    private fun shouldFetch(state: MerchantDetailsViewState?): Boolean =
        state !is MerchantDetailsViewState.Loading && state !is MerchantDetailsViewState.Success

    private fun handleError(error: Throwable) {
        error.printStackTrace()
        _viewState.value =
            MerchantDetailsViewState.Failure(listOf(ViewElement.Error(error.localizedMessage)))
    }
}


