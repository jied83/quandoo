package ru.jied83.test.ui

import android.content.Context
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT

fun Context.getOrientation() =
    this.resources.configuration.orientation
        .takeIf { it != ORIENTATION_PORTRAIT }
        ?.let { ORIENTATION_LANDSCAPE }
        ?: ORIENTATION_PORTRAIT
