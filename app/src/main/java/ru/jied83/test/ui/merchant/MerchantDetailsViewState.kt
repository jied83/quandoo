package ru.jied83.test.ui.merchant

import ru.jied83.test.ui.common.ViewElement

sealed class MerchantDetailsViewState {

    abstract val elements: List<ViewElement>

    data class Loading(
        override val elements: List<ViewElement>
    ) : MerchantDetailsViewState()

    data class Success(
        override val elements: List<ViewElement>
    ) : MerchantDetailsViewState()

    data class Failure(
        override val elements: List<ViewElement>
    ) : MerchantDetailsViewState()
}