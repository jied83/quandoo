package ru.jied83.test.ui

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

open class ViewModelScope : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext get() = viewModelContext + Dispatchers.Main
    private val viewModelContext: Job = SupervisorJob()

    override fun onCleared() {
        cancel()
    }

    protected fun createChildContext(): Job {
        return SupervisorJob(viewModelContext)
    }

    protected fun launchImmediate(block: suspend CoroutineScope.() -> Unit): Job {
        return launch(context = Dispatchers.Main.immediate, block = block)
    }
}