package ru.jied83.test.ui.merchants.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.jied83.test.R
import ru.jied83.test.ui.common.Adapter
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.common.urlCallback

class MechantAdapterDelegate(
    private val urlCallback: urlCallback
) : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is ViewElement.Merchant
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
        val merchantHolder = holder as MerchantItemViewHolder
        val merchantElement = list[position] as ViewElement.Merchant
        merchantHolder.bind(merchantElement)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MerchantItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_merchant,
                parent,
                false
            ),
            urlCallback
        )
    }

    class MerchantItemViewHolder(
        view: View,
        private val urlCallback: urlCallback
    ) : RecyclerView.ViewHolder(view) {

        private val title: TextView = view.findViewById(R.id.title)
        private val cover: ImageView = view.findViewById(R.id.cover)
        private val glide = Glide.with(view.context)
        private val requestOptions = RequestOptions().apply {
            placeholder(R.drawable.image_placeholder)
            error(R.drawable.image_placeholder)
        }

        fun bind(data: ViewElement.Merchant) {
            title.text = data.name
            data.imageUrl?.let { url ->
                glide.load(url).apply(requestOptions).into(cover)
            } ?: cover.setImageResource(R.drawable.image_placeholder)
            itemView.setOnClickListener { urlCallback(data.id) }
        }
    }
}