package ru.jied83.test.ui.merchant.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.R
import ru.jied83.test.ui.common.Adapter
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.merchant.MerchantInfo

class MerchantReviewAdapterDelegate : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is MerchantInfo.Review
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
        val merchantReviewHolder = holder as MerchantReviewItemViewHolder
        val merchantReviewElement = list[position] as MerchantInfo.Review
        merchantReviewHolder.bind(merchantReviewElement)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MerchantReviewItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_merchant_review,
                parent,
                false
            )
        )
    }

    class MerchantReviewItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val name: TextView = view.findViewById(R.id.name)
        private val desc: TextView = view.findViewById(R.id.description)
        private val rating: TextView = view.findViewById(R.id.rating)

        fun bind(data: MerchantInfo.Review) {
            name.text = data.name
            desc.text = data.description
            rating.text = data.rating
        }
    }
}