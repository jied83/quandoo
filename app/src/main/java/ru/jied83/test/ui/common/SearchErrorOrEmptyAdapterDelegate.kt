package ru.jied83.test.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.R

class SearchErrorOrEmptyAdapterDelegate : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is ViewElement.Error
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
        val errHolder = holder as ErrorItemViewHolder
        val errElement = list[position] as ViewElement.Error
        errHolder.bind(errElement)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ErrorItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_empty_or_error,
                parent,
                false
            )
        )
    }

    class ErrorItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.title)
        fun bind(data: ViewElement.Error) {
            title.text = data.msg
        }
    }
}