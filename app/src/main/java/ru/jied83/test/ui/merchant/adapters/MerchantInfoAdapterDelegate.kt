package ru.jied83.test.ui.merchant.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.R
import ru.jied83.test.ui.common.Adapter
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.merchant.MerchantInfo

class MerchantInfoAdapterDelegate : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is MerchantInfo.Information
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
        val merchantInfoHolder = holder as MerchantInfoItemViewHolder
        val merchantInfoElement = list[position] as MerchantInfo.Information
        merchantInfoHolder.bind(merchantInfoElement)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MerchantInfoItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_merchant_info_information,
                parent,
                false
            )
        )
    }

    class MerchantInfoItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val name: TextView = view.findViewById(R.id.title)
        private val addressLine: TextView = view.findViewById(R.id.address)
        private val cityLine: TextView = view.findViewById(R.id.zip_city_contry)
        private val rating: TextView = view.findViewById(R.id.rating)

        fun bind(data: MerchantInfo.Information) {
            name.text = data.name

            data.addressLine.takeIf { !it.isNullOrBlank() }?.let {
                addressLine.visibility = View.VISIBLE
                addressLine.text = it
            } ?: run { addressLine.visibility = View.GONE }

            data.cityLine.takeIf { !it.isNullOrBlank() }?.let {
                cityLine.visibility = View.VISIBLE
                cityLine.text = it
            } ?: run { cityLine.visibility = View.GONE }

            rating.text = data.rating
        }
    }
}