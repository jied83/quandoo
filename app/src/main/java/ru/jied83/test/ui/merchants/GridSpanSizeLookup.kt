package ru.jied83.test.ui.merchants

import android.content.res.Configuration
import androidx.recyclerview.widget.GridLayoutManager

class GridSpanSizeLookup(private val spanSize: Int) : GridLayoutManager.SpanSizeLookup() {
    override fun getSpanSize(position: Int) =
        position.takeIf { predicate(it) }?.let { spanSize } ?: 1
}

class SpanSizeCalculator {

    companion object {
        private const val SPAN_SIZE_PORTRAIT = 3
        private const val SPAN_SIZE_LANDSCAPE = 3
    }

    fun calc(orientation: Int) = when (orientation) {
        Configuration.ORIENTATION_PORTRAIT -> SPAN_SIZE_PORTRAIT
        else -> SPAN_SIZE_LANDSCAPE
    }
}