package ru.jied83.test.ui.merchants

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.jied83.test.AuthException
import ru.jied83.test.interactor.MerchantInteractor
import ru.jied83.test.interactor.MerchantRequest
import ru.jied83.test.ui.SingleLiveDataEvent
import ru.jied83.test.ui.ViewModelScope
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.navigation.NavigationRequest

class MerchantsViewModel(
    private val merchantInteractor: MerchantInteractor,
    private val merchantsViewElementMapper: MerchantsViewElementMapper
) : ViewModelScope() {
    val searchViewState: LiveData<SearchViewState> get() = _searchViewState
    val navigationRequest: SingleLiveDataEvent<NavigationRequest> get() = _navigationRequest

    private val _searchViewState = MutableLiveData<SearchViewState>()
    private val _navigationRequest = SingleLiveDataEvent<NavigationRequest>()
    private val searchContext = createChildContext()

    init {
        onSearchInput("")
    }

    fun onItemClick(url: String) {
        launchImmediate {
            _navigationRequest.setValue(NavigationRequest.showMerchantInfo(url))
        }
    }

    fun onLoadMore() {
        launchImmediate {
            if (couldLoadMore(searchViewState.value)) {
                val lastState = searchViewState.value as SearchViewState.SearchSuccess
                _searchViewState.value =
                    SearchViewState.LoadingProcess(
                        lastState.elements + listOf(ViewElement.Progress),
                        lastState.search,
                        false
                    )
                runCatching {
                    withContext(searchContext + Dispatchers.IO) {
                        merchantInteractor.fetchMerchants(
                            MerchantRequest.Query(
                                lastState.search,
                                lastState.offset + MerchantRequest.PAGE_SIZE
                            )
                        )
                    }
                }.onFailure {
                    handleError(it)
                }.onSuccess { result ->
                    _searchViewState.value =
                        SearchViewState.SearchSuccess(
                            lastState.elements + merchantsViewElementMapper(result.merchants),
                            lastState.search,
                            result.offset,
                            result.count
                        )
                }
            }
        }
    }

    fun onSearchInput(search: String) {
        val lastState = searchViewState.value

        if (shouldStartSearch(search, lastState))
            launchImmediate {
                _searchViewState.value =
                    SearchViewState.LoadingProcess(listOf(ViewElement.Progress), search, true)

                runCatching {
                    withContext(Dispatchers.IO) {
                        merchantInteractor.fetchMerchants(MerchantRequest.Query(search, 0))
                    }
                }.onFailure {
                    handleError(it)
                    _searchViewState.value =
                        SearchViewState.SearchFailure(
                            listOf(ViewElement.Error(it.localizedMessage)),
                            search
                        )
                }.onSuccess {
                    _searchViewState.value =
                        if (it.merchants.isNotEmpty())
                            SearchViewState.SearchSuccess(
                                merchantsViewElementMapper(it.merchants),
                                search,
                                it.offset,
                                it.count
                            )
                        else
                            SearchViewState.SearchFailure(
                                listOf(ViewElement.Error("No restaurants found: $search")), search
                            )
                }
            }
    }

    private fun couldLoadMore(state: SearchViewState?) =
        when (state) {
            is SearchViewState.SearchSuccess -> state.elements.size < state.count
            else -> false
        }

    private fun shouldStartSearch(search: String, lastState: SearchViewState?) =
        search != lastState?.search.orEmpty() || lastState == null

    private fun handleError(error: Throwable) {
        error.printStackTrace()
        when (error) {
            is AuthException -> {
            }
            else -> {
            }
        }
    }

}


