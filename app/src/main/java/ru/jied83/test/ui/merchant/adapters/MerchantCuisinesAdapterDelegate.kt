package ru.jied83.test.ui.merchant.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.R
import ru.jied83.test.ui.common.Adapter
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.merchant.MerchantInfo


class MerchantCuisinesAdapterDelegate : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is MerchantInfo.Cuisine
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
        val merchantCuisineHolder = holder as MerchantCuisineItemViewHolder
        val merchantCuisineElement = list[position] as MerchantInfo.Cuisine
        merchantCuisineHolder.bind(merchantCuisineElement)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MerchantCuisineItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_merchant_cousines,
                parent,
                false
            )
        )
    }

    class MerchantCuisineItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val layout: LinearLayout = view.findViewById(R.id.layout)

        fun bind(data: MerchantInfo.Cuisine) {
            val views = scrapViews(data.cousines, itemView.resources.displayMetrics.density)
            layout.removeAllViews()
            views.forEach {
                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                )
                layoutParams.setMargins(8, 0, 0, 0)
                layout.addView(it, layoutParams)
            }
        }

        private fun scrapViews(data: List<String>, density: Float): List<View> = data.map { txt ->
            TextView(itemView.context).apply {
                text = txt
                textSize = 16F
                setTextColor(Color.WHITE)
                setBackgroundResource(R.drawable.chip_background)
                setPadding(
                    paddingLeft + (8 * density).toInt(),
                    paddingTop + (8 * density).toInt(),
                    paddingRight + (8 * density).toInt(),
                    paddingBottom + (8 * density).toInt()
                )
            }
        }
    }
}