package ru.jied83.test.ui.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.androidx.scope.currentScope
import ru.jied83.test.R
import ru.jied83.test.ui.common.ViewElement
import ru.jied83.test.ui.merchant.adapters.MerchantInfoRecyclerAdapter

class MerchantDetailsFragment : Fragment() {

    companion object {

        const val EXTRA_MERCHANT_ID = "extra_merchant_id"

        fun newInstance(id: String): MerchantDetailsFragment = MerchantDetailsFragment().apply {
            arguments = Bundle().apply { putString(EXTRA_MERCHANT_ID, id) }
        }
    }

    private lateinit var viewModel: MerchantDetailsViewModel
    private lateinit var recyclerAdapter: MerchantInfoRecyclerAdapter
    private val viewModelFactory: MerchantDetailViewModelFactory by currentScope.inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_merchant_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val id =
            arguments?.getString(EXTRA_MERCHANT_ID) ?: throw IllegalStateException("No id provided")

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(MerchantDetailsViewModel::class.java)
        setupRecyclerview()
        setUpToolbar()
        viewModel.viewState.observe(viewLifecycleOwner, Observer(::applyState))
        viewModel.onCreate(id)
    }


    private fun setupRecyclerview() {
        recyclerAdapter = MerchantInfoRecyclerAdapter()
        val layoutManager = LinearLayoutManager(requireContext())
        recycler.apply {
            this.layoutManager = layoutManager
            this.adapter = recyclerAdapter
        }
    }

    private fun applyState(viewstate: MerchantDetailsViewState) {
        updateList(viewstate.elements)
    }

    private fun setUpToolbar() {
        with(requireActivity() as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.run {
                setDisplayHomeAsUpEnabled(true)
                title = ""
            }
        }
    }

    private fun updateList(elements: List<ViewElement>) {
        recyclerAdapter.set(elements)
    }
}
