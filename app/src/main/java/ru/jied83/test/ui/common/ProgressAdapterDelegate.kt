package ru.jied83.test.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.R

class ProgressAdapterDelegate : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is ViewElement.Progress
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ProgressItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_progress,
                parent,
                false
            )
        )
    }

    class ProgressItemViewHolder(view: View) : RecyclerView.ViewHolder(view)
}