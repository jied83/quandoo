package ru.jied83.test.ui.common

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

typealias urlCallback = (String) -> Unit

interface Adapter<T : ViewElement> {

    fun isForViewType(list: List<T>, position: Int): Boolean
    fun onBindViewHolder(list: List<T>, position: Int, holder: RecyclerView.ViewHolder)
    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
}

sealed class ViewElement {

    data class Merchant(
        val id: String,
        val name: String?,
        val imageUrl: String?
    ) : ViewElement()

    data class Error(val msg: String?) : ViewElement()
    object Progress : ViewElement()
    data class SectionHeader(val title: String) : ViewElement()
    abstract class Extended : ViewElement()
}