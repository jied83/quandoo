package ru.jied83.test.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.jied83.test.R

class SectionHeaderAdapterDelegate : Adapter<ViewElement> {

    override fun isForViewType(list: List<ViewElement>, position: Int): Boolean {
        return list[position] is ViewElement.SectionHeader
    }

    override fun onBindViewHolder(
        list: List<ViewElement>,
        position: Int,
        holder: RecyclerView.ViewHolder
    ) {
        val sectionHolder = holder as SectionItemViewHolder
        val sectionElement = list[position] as ViewElement.SectionHeader
        sectionHolder.bind(sectionElement)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return SectionItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_section_header,
                parent,
                false
            )
        )
    }

    class SectionItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val section: TextView = view.findViewById(R.id.title)

        fun bind(data: ViewElement.SectionHeader) {
            section.text = data.title
        }
    }
}