package ru.jied83.test.data.remote.dto

import com.google.gson.annotations.SerializedName


data class MerchantInformationTo(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("images") val images: List<ImageTo>?,
    @SerializedName("location") val location: LocationTo?,
    @SerializedName("reviewScore") val reviewScore: String?,
    @SerializedName("links") val links: List<LinkTo>,
    @SerializedName("tagGroups") val tags: List<TagContainerTo>?
)

data class LocationTo(
    @SerializedName("coordinates") val coordinates: CoordinatesTo?,
    @SerializedName("address") val address: AddressTo?
)

data class TagContainerTo(
    @SerializedName("type") val type: String,
    @SerializedName("tags") val tags: List<TagTo>
)

data class TagTo(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String
)

data class CoordinatesTo(
    @SerializedName("latitude") val lat: Double,
    @SerializedName("longitude") val lng: Double
)

data class AddressTo(
    @SerializedName("country") val country: String?,
    @SerializedName("street") val street: String?,
    @SerializedName("zipcode") val zipcode: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("number") val number: String?
)

data class LinkTo(
    @SerializedName("href") val href: String?,
    @SerializedName("method") val method: String?,
    @SerializedName("rel") val rel: String?
)