package ru.jied83.test.data.remote

import retrofit2.HttpException
import ru.jied83.test.AuthException
import ru.jied83.test.data.DataSource
import ru.jied83.test.data.remote.mappers.MerchantInfoMapper
import ru.jied83.test.data.remote.mappers.ReviewMapper
import ru.jied83.test.data.remote.mappers.SearchResultMapper
import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.Merchants
import ru.jied83.test.model.Review

class RemoteDataSource(private val api: QuandooApiEndpoints) : DataSource {

    private val searchResultMapper = SearchResultMapper()
    private val merchantInfoMapper = MerchantInfoMapper()
    private val reviewMapper = ReviewMapper()

    override suspend fun fetchMerchants(query: String?, offset: Int, limit: Int): Merchants =
        runCatching { api.fetchMerchants(query, offset, limit) }
            .mapCatching { response -> searchResultMapper(response, query) }
            .onFailure { e -> onFailure(e) }
            .getOrThrow()

    override suspend fun fetchMerchantInfo(id: String): MerchantDetails =
        runCatching { api.fetchMerchantById(id) }
            .mapCatching { resp -> merchantInfoMapper(resp) }
            .onFailure { e -> onFailure(e) }
            .getOrThrow()

    override suspend fun fetchReviews(id: String): List<Review> =
        runCatching { api.fetchReviewsForMerchant(id, 0, 5) }
            .mapCatching { resp -> reviewMapper(resp) }
            .onFailure { e -> onFailure(e) }
            .getOrThrow()

    private fun onFailure(e: Throwable) {
        if (e is HttpException && e.code() == 401)
            throw AuthException(e.message())
        else throw e
    }
}



