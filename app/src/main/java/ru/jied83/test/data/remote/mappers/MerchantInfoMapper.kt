package ru.jied83.test.data.remote.mappers

import ru.jied83.test.data.remote.dto.MerchantInformationTo
import ru.jied83.test.model.Address
import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.TagType

class MerchantInfoMapper : (MerchantInformationTo) -> MerchantDetails {

    override fun invoke(to: MerchantInformationTo): MerchantDetails =
        MerchantDetails(
            id = to.id,
            name = to.name,
            images = to.images?.map { it.url },
            address = to.location?.address?.run {
                Address(country, zipcode, city, "${street.orEmpty()} ${number.orEmpty()}".trim())
            },
            rating = to.reviewScore,
            infoLink = to.links.firstOrNull { it.rel == "details" }?.href,
            cuisines = to.tags?.firstOrNull {
                it.type == TagType.CUISINE.toString()
            }?.tags?.map { it.name }
        )
}