package ru.jied83.test.data.remote

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.jied83.test.data.remote.dto.MerchantInformationTo
import ru.jied83.test.data.remote.dto.MerchantsResponse
import ru.jied83.test.data.remote.dto.ReviewResponse

interface QuandooApiEndpoints {

    @GET("v1/merchants")
    suspend fun fetchMerchants(
        @Query("query") search: String?,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): MerchantsResponse

    @GET("v1/merchants/{id}")
    suspend fun fetchMerchantById(
        @Path("id") id: String
    ): MerchantInformationTo

    @GET("v1/review")
    suspend fun fetchReviewsForMerchant(
        @Query("merchantId") merchantId: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): ReviewResponse
}