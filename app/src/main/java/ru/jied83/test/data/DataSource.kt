package ru.jied83.test.data

import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.Merchants
import ru.jied83.test.model.Review


interface DataSource {

    suspend fun fetchMerchants(query: String?, offset: Int, limit: Int): Merchants
    suspend fun fetchMerchantInfo(id: String): MerchantDetails
    suspend fun fetchReviews(id: String): List<Review>
}