package ru.jied83.test.data.remote.dto

import com.google.gson.annotations.SerializedName


data class MerchantsResponse(
    @SerializedName("merchants") val merchants: List<MerchantTo>,
    @SerializedName("offset") val offset: Int,
    @SerializedName("size") val count: Int,
    @SerializedName("limit") val limit: Int
)

data class MerchantTo(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("images") val images: List<ImageTo>
)

data class ImageTo(
    @SerializedName("url") val url: String
)
