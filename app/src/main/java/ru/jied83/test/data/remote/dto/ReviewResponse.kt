package ru.jied83.test.data.remote.dto

import com.google.gson.annotations.SerializedName

data class ReviewResponse(
    @SerializedName("reviews") val reviews: List<ReviewTo>,
    @SerializedName("offset") val offset: Int,
    @SerializedName("size") val count: Int,
    @SerializedName("limit") val limit: Int
)

data class ReviewTo(
    @SerializedName("reviewId") val id: String,
    @SerializedName("merchantId") val merchantId: String,
    @SerializedName("customerFirstName") val firstName: String,
    @SerializedName("customerLastName") val lastName: String,
    @SerializedName("rating") val rating: Double,
    @SerializedName("description") val description: String
)