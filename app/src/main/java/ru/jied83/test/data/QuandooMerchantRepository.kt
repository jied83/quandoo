package ru.jied83.test.data

import ru.jied83.test.data.remote.RemoteDataSource
import ru.jied83.test.interactor.MerchantRepository
import ru.jied83.test.model.MerchantDetails
import ru.jied83.test.model.Review

class QuandooMerchantRepository(
    private val remoteDataSource: RemoteDataSource
) : MerchantRepository {

    override suspend fun fetchMerchants(query: String?, offset: Int, count: Int) =
        remoteDataSource.fetchMerchants(query, offset, count)

    override suspend fun fetchMerchantDetails(id: String): MerchantDetails =
        remoteDataSource.fetchMerchantInfo(id)

    override suspend fun fetchReview(id: String): List<Review> =
        remoteDataSource.fetchReviews(id)
}