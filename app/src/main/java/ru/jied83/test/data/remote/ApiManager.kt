package ru.jied83.test.data.remote

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiManager {
    companion object {
        private const val CONNECTION_TIMEOUT = 30000
        private const val READ_TIMEOUT = 15000

        fun loggingInterceptor(logLevel: HttpLoggingInterceptor.Level): Interceptor {
            return HttpLoggingInterceptor().setLevel(logLevel)
        }
    }

    fun getClient(interceptors: List<Interceptor>): OkHttpClient {
        val builder = clientBuilder()
        interceptors.forEach { builder.addInterceptor(it) }
        return builder.build()
    }

    private fun clientBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
    }

    fun <T> getService(host: String, client: OkHttpClient, clazz: Class<T>): T {
        return Retrofit.Builder().baseUrl(host).client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(clazz)
    }
}