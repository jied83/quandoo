package ru.jied83.test.data.remote.mappers

import ru.jied83.test.data.remote.dto.MerchantsResponse
import ru.jied83.test.model.Merchant
import ru.jied83.test.model.Merchants

class SearchResultMapper : (MerchantsResponse, String?) -> Merchants {

    override fun invoke(response: MerchantsResponse, query: String?): Merchants =
        Merchants(
            query = query,
            offset = response.offset,
            count = response.count,
            merchants = response.merchants.map { to ->
                Merchant(to.id, to.name, to.images.map { it.url })
            })
}