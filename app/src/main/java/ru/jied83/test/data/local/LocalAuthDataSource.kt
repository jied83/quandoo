package ru.jied83.test.data.local

import android.content.SharedPreferences

class LocalAuthDataSource(private val prefs: SharedPreferences) {

    companion object {
        private const val SESSION_TOKEN = "session_token"
    }

    var sessionToken: String
        get() = prefs.getString(SESSION_TOKEN, "token").orEmpty()
        set(value) {
            prefs.edit().putString(SESSION_TOKEN, value).apply()
        }

    fun clearSessionToken() {
        prefs.edit().remove(SESSION_TOKEN).apply()
    }
}