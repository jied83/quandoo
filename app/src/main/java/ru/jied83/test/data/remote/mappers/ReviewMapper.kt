package ru.jied83.test.data.remote.mappers

import ru.jied83.test.data.remote.dto.ReviewResponse
import ru.jied83.test.model.Review

class ReviewMapper : (ReviewResponse) -> List<Review> {

    override fun invoke(response: ReviewResponse): List<Review> =
        response.reviews.map { to ->
            Review(
                name = "${to.firstName} ${to.lastName}",
                rating = to.rating,
                description = to.description
            )
        }
}