package ru.jied83.test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.koin.android.ext.android.inject
import ru.jied83.test.R
import ru.jied83.test.ui.navigation.Navigation
import ru.jied83.test.ui.navigation.ActivityHolder

class MainActivity : AppCompatActivity() {

    private val navigation: Navigation by inject()
    private val activityHolder: ActivityHolder by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityHolder.attachActivity(this)
        if (savedInstanceState == null)
            navigation.merchants()

    }

    override fun onStart() {
        super.onStart()
        activityHolder.attachActivity(this)
    }

    override fun onStop() {
        super.onStop()
        activityHolder.detachActivity()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
