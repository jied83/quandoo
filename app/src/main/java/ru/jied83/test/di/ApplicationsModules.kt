package ru.jied83.test.di

import android.preference.PreferenceManager
import com.google.gson.Gson
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.jied83.test.data.local.LocalAuthDataSource
import ru.jied83.test.data.QuandooMerchantRepository
import ru.jied83.test.data.remote.ApiManager
import ru.jied83.test.data.remote.QuandooApiEndpoints
import ru.jied83.test.data.remote.RemoteDataSource
import ru.jied83.test.interactor.MerchantInteractor
import ru.jied83.test.interactor.MerchantRepository
import ru.jied83.test.interactor.QuandooMerchantInteractor
import ru.jied83.test.ui.merchant.MerchantDetailViewModelFactory
import ru.jied83.test.ui.merchant.MerchantDetailsFragment
import ru.jied83.test.ui.merchant.MerchantInfoViewElementMapper
import ru.jied83.test.ui.merchant.MerchantReviewViewElementMapper
import ru.jied83.test.ui.merchants.MerchantsFragment
import ru.jied83.test.ui.merchants.MerchantsViewElementMapper
import ru.jied83.test.ui.merchants.MerchantsViewModelFactory
import ru.jied83.test.ui.navigation.ActivityHolder
import ru.jied83.test.ui.navigation.Navigation
import ru.jied83.test.ui.navigation.TestNavigation


val application = module {
    single { Gson() }
    single { PreferenceManager.getDefaultSharedPreferences(androidContext()) }
    single { LocalAuthDataSource(get()) }
    /// API
    factory(named("API")) {
        ApiManager().getClient(
            listOf(ApiManager.loggingInterceptor(HttpLoggingInterceptor.Level.BODY))
        )
    }

    single {
        ApiManager().getService(
            "https://test-9250-api.quandoo.com",
            get(named("API")),
            QuandooApiEndpoints::class.java
        )
    }

    scope(named<MerchantsFragment>()) {
        scoped { RemoteDataSource(get()) }
        scoped<MerchantRepository> { QuandooMerchantRepository(get()) }
        scoped<MerchantInteractor> { QuandooMerchantInteractor(get()) }
        scoped { MerchantsViewModelFactory(get(), get()) }
    }

    scope(named<MerchantDetailsFragment>()) {
        scoped { RemoteDataSource(get()) }
        scoped<MerchantRepository> { QuandooMerchantRepository(get()) }
        scoped<MerchantInteractor> { QuandooMerchantInteractor(get()) }
        scoped { MerchantDetailViewModelFactory(get(), get(), get()) }
    }
}

val navigation = module {
    single { ActivityHolder() }
    single<Navigation> { TestNavigation(get()) }
}

val ui = module {
    factory { MerchantsViewElementMapper() }
    factory { MerchantInfoViewElementMapper() }
    factory { MerchantReviewViewElementMapper() }
}